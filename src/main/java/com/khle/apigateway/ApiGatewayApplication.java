package com.khle.apigateway;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.WebApplicationInitializer;

/**
 * @author khle
 *
 */
@SpringBootApplication
@EnableScheduling
public class ApiGatewayApplication extends SpringBootServletInitializer
		implements WebApplicationInitializer {
	private static final String SPRING_CONFIG_LOCATION = "spring.config.location";
	private static final Logger logger = LogManager.getLogger(ApiGatewayApplication.class);
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder springApplicationBuilder) {
		return springApplicationBuilder.sources(ApiGatewayApplication.class).properties(getProperties());
	}

	public static void main(String[] args) {
		SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder(ApiGatewayApplication.class);
		
		springApplicationBuilder.sources(ApiGatewayApplication.class)
                .properties(getProperties())
                .run(args);
	}	

	static Properties getProperties() {
		Properties props = new Properties();
		props.put(SPRING_CONFIG_LOCATION, "classpath:apigateway/conf/");	
		logger.info(props.get(SPRING_CONFIG_LOCATION));
		return props;
	}
}
