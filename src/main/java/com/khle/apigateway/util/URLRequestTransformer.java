package com.khle.apigateway.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.RequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.khle.apigateway.configuration.ApiGatewayProperties;

public class URLRequestTransformer extends ProxyRequestTransformer {

	private ApiGatewayProperties apiGatewayProperties;

	public URLRequestTransformer(ApiGatewayProperties apiGatewayProperties, String allowServices) {
		this.apiGatewayProperties = apiGatewayProperties;
	}

	@Override
	public RequestBuilder transform(HttpServletRequest request)
			throws URISyntaxException, IOException, NoHandlerFoundException {
		String contextPath = request.getContextPath();
		String requestURI = (contextPath == null || contextPath.isEmpty()) ? request.getRequestURI()
				: request.getRequestURI().replaceFirst(contextPath, "");
		URI uri = new URI(getServiceUrl(requestURI, request));
//		if (request.getQueryString() != null && !request.getQueryString().isEmpty()) {
//			uri = new URI(getServiceUrl(requestURI, request) + "?" + request.getQueryString());
//		} else {
//			uri = new URI(getServiceUrl(requestURI, request));
//		}

		RequestBuilder rb = RequestBuilder.create(request.getMethod());
		rb.setUri(uri);
		return rb;
	}

	private String getServiceUrl(String requestURI, HttpServletRequest httpServletRequest)
			throws NoHandlerFoundException {
		String location = "";
		for (int i = 0; i < apiGatewayProperties.getEndpoints().size(); i++) {
			ApiGatewayProperties.Endpoint endPoint = apiGatewayProperties.getEndpoints().get(i);
			if(endPoint.getSub().isEmpty() &&
					(requestURI.matches(endPoint.getPath()) && endPoint.getMethod() == RequestMethod.valueOf(httpServletRequest.getMethod()))) {
				location = endPoint.getLocation();
				break;
			}
			else {
				for (int j = 0; j < endPoint.getSub().size(); j++) {
					ApiGatewayProperties.Endpoint sub = endPoint.getSub().get(j);
					if(requestURI.matches(endPoint.getPath() + sub.getPath()) && sub.getMethod() == RequestMethod.valueOf(httpServletRequest.getMethod())) {
						location = sub.getLocation();
						break;
					}
					else if(!location.isEmpty()) break;
				}
			}
		}
//		validateSupportedService(service);
		if(location.isEmpty()) throw new NoHandlerFoundException(httpServletRequest.getMethod(), requestURI, null);
		return location + requestURI;
	}

	/*private void validateSupportedService(String service) {
		Set<String> services = new HashSet<>();
		if (allowServices != null) {
			for (String s : allowServices.split("\\s")) {
				services.add(s);
			}
			if (!services.contains(service)) {
				throw new InvalidTokenException("Service is not supported for this client");
			}
		}
	}*/
}
