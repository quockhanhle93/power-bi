package com.khle.apigateway.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.RequestBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.NoHandlerFoundException;
@Component
public class HeadersRequestTransformer extends ProxyRequestTransformer {
	
	@Override
	public RequestBuilder transform(HttpServletRequest request)
			throws URISyntaxException, IOException, NoHandlerFoundException {
		RequestBuilder requestBuilder = predecessor.transform(request);

		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerValue = request.getHeader(headerName);

			if (headerName.equals("x-access-token")) {
				requestBuilder.removeHeaders(headerName);
				requestBuilder.addHeader(headerName, headerValue);
			}
		}
		
		requestBuilder.removeHeaders(HttpHeaders.AUTHORIZATION);
		String auth = "api:Tccvn@2019";
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
		
		String authHeader = "Basic " + new String(encodedAuth);
		requestBuilder.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

		return requestBuilder;
	}
}
