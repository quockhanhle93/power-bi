package com.khle.apigateway.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.springframework.web.servlet.NoHandlerFoundException;

public class ContentRequestTransformer extends ProxyRequestTransformer {

  @Override
  public RequestBuilder transform(HttpServletRequest request) throws URISyntaxException, IOException, NoHandlerFoundException {
    RequestBuilder requestBuilder = predecessor.transform(request);
    String postedData = "";
    BufferedReader in = new BufferedReader(new InputStreamReader(
    				request.getInputStream()));
    String line = in.readLine();
    while (line != null) {
    	postedData += line;
    	line = in.readLine();
    }
    if (!postedData.isEmpty()) {
      StringEntity entity = new StringEntity(postedData, ContentType.APPLICATION_JSON);
      requestBuilder.setEntity(entity);
    }
    Enumeration<String> paremeterNames = request.getParameterNames();
    while (paremeterNames.hasMoreElements()) {
      String parameterName = paremeterNames.nextElement();
      String parameterValue = request.getParameter(parameterName);
      requestBuilder.addParameter(parameterName, parameterValue);
    }
    return requestBuilder;
  }
}
