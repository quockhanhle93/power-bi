package com.khle.apigateway.service;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.khle.apigateway.model.Content;

public interface TaskTimerRepository extends MongoRepository<Content, Serializable>{
	
}
