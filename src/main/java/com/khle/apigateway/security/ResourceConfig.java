package com.khle.apigateway.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;

//@Configuration
//@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {
	
	@Value("${security.oauth2.software.id}")
    private String softwareId;

	@Value("${security.oauth2.resource.id}")
    private String resourceId;
    
//    @Value("${security.oauth2.token.endpoint}")
//    private String tokenEndpoint;
    
    @Value("${security.oauth2.client.id}")
    private String clientId;
    
    @Value("${security.oauth2.client.secret}")
    private String clientSecret;

    // To allow the rResourceServerConfigurerAdapter to understand the token,
    // it must share the same characteristics with AuthorizationServerConfigurerAdapter.
    // So, we must wire it up the beans in the ResourceServerSecurityConfigurer.
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources
                .resourceId(resourceId);

    }
    
    @Override
    public void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
                http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                    .and()
                    .authorizeRequests().anyRequest().authenticated();
        // @formatter:on                
    }

    @Bean
    public AccessTokenConverter accessTokenConverter() {
        return new DefaultAccessTokenConverter();
    }
    
    @Primary
    @Bean
    public CustomRemoteTokenService tokenServices() {
        final CustomRemoteTokenService tokenService = new CustomRemoteTokenService();
//        tokenService.setCheckTokenEndpointUrl(tokenEndpoint);
        tokenService.setAccessTokenConverter(accessTokenConverter());
        tokenService.setSoftwareId(softwareId);
        return tokenService;
    }
    

}
