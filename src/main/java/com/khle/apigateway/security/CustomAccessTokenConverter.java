package com.khle.apigateway.security;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.stereotype.Component;

//@Component
public class CustomAccessTokenConverter extends DefaultAccessTokenConverter {

	private static final String TRUSTED_CLIENT = "trusted_client";
	private static final String ALLOWED_SERVICES = "allowed_services";
	
	@Value("${security.oauth2.software.id}")
    private String softwareId;

	@Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> claims) {
		Set<String> services = new HashSet<>();
		if (claims.get(ALLOWED_SERVICES) != null) {
			for (String s : claims.get(ALLOWED_SERVICES).toString().split("\\s")) {
				services.add(s);
			}
			if (!services.contains(softwareId)) {
				return null;
			}
		}
		
		OAuth2Authentication authentication = super.extractAuthentication(claims);
		if (authentication.getAuthorities() == null || authentication.getAuthorities().isEmpty() || !authentication.getAuthorities().contains(new SimpleGrantedAuthority(TRUSTED_CLIENT))) {
        	return null;
		}
		
        authentication.setDetails(claims);
        return authentication;
    }

}
