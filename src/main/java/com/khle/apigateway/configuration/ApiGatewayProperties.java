package com.khle.apigateway.configuration;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMethod;

@ConfigurationProperties(prefix = "api.gateway")
public class ApiGatewayProperties {

	private List<Endpoint> endpoints;

	public static class Endpoint {
		private String path;
		private List<Endpoint> sub;
		private RequestMethod method;
		private String location;
		private String service;

		public Endpoint() {
		}

		public Endpoint(String location) {
			this.location = location;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public RequestMethod getMethod() {
			return method;
		}

		public void setMethod(RequestMethod method) {
			this.method = method;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public List<Endpoint> getSub() {
			return sub;
		}

		public void setSub(List<Endpoint> sub) {
			this.sub = sub;
		}

		public String getService() {
			return service;
		}

		public void setService(String service) {
			this.service = service;
		}
	}

	public List<Endpoint> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<Endpoint> endpoints) {
		this.endpoints = endpoints;
	}
}
