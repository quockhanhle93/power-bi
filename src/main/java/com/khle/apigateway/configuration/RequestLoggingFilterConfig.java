package com.khle.apigateway.configuration;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

//@Configuration
public class RequestLoggingFilterConfig extends CommonsRequestLoggingFilter  {
	private static final Logger logger = LogManager.getLogger(RequestLoggingFilterConfig.class);
	@Override
	protected boolean shouldLog(HttpServletRequest request) {
		return logger.isDebugEnabled(); 
	}

	/**
	 * Writes a log message before the request is processed.
	 */
	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		logger.debug(message);
	}

	/**
	 * Writes a log message after the request is processed.
	 */
	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		logger.debug(message);
	}
	
	@Bean
	public RequestLoggingFilterConfig logFilter() {
		RequestLoggingFilterConfig filter
                = new RequestLoggingFilterConfig();
		filter.setIncludeClientInfo(true);
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setIncludeHeaders(true);

        return filter;
    }
	
   
}
