package com.khle.apigateway.configuration;

import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties({ ApiGatewayProperties.class })
public class ApiGatewayServiceConfiguration extends WebMvcConfigurerAdapter {
	
	@Bean
	public AsyncRestTemplate restTemplate(HttpMessageConverters converters) {

		// we have to define Apache HTTP client to use the PATCH verb
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/json"));
		converter.setObjectMapper(new ObjectMapper());

		AsyncRestTemplate restTemplate = getAsyncRestTemplate();

		restTemplate.setErrorHandler(new RestTemplateErrorHandler());

		return restTemplate;
	}

	private AsyncRestTemplate getAsyncRestTemplate() {
		HttpComponentsAsyncClientHttpRequestFactory asyncRequestFactory = new HttpComponentsAsyncClientHttpRequestFactory();
		asyncRequestFactory.setAsyncClient(createAsyncHttpClient());
		return new AsyncRestTemplate(asyncRequestFactory);
	}
	
	@SuppressWarnings("unused")
	private RestTemplate getSyncRestTemplate() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(HttpClients.createDefault());
		return new RestTemplate(requestFactory);
	}

	private CloseableHttpAsyncClient createAsyncHttpClient() {
		int connectionTimeout = 3000000;

		IOReactorConfig ioReactorConfig = IOReactorConfig.custom().setConnectTimeout(connectionTimeout)
				.setSoTimeout(connectionTimeout).setIoThreadCount(20).build();

		return HttpAsyncClients.custom().setDefaultIOReactorConfig(ioReactorConfig).build();
	}
}
