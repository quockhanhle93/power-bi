package com.khle.apigateway.model;

public enum Role {
    ROLE_USER ("user"),
    ROLE_ADMIN ("admin"),
    ROLE_REGISTER ("register"),
    ROLE_TRUSTED_CLIENT ("trusted_client"),
    ROLE_CLIENT ("client");
	
	private String role;
	
	Role(String role) {
        this.role = role;
    }
 
    public String getRole() {
        return role;
    }
    
    @Override
    public String toString() {
        return role;
    }
}
