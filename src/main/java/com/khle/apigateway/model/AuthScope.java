package com.khle.apigateway.model;

public enum AuthScope {
	READ ("read"),
	WRITE ("write"),
	REGISTER ("register"),
	TRUST ("trust");
	
	private String scope;
	
	AuthScope(String scope) {
        this.scope = scope;
    }
 
    public String getScope() {
        return scope;
    }
    
    @Override
    public String toString() {
        return scope;
    }
}
