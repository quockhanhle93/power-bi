package com.khle.apigateway.model;

public enum AuthorizedGrantType {
	CLIENT_CREDENTIALS("client_credentials"),
	PASSWORD("password"),
	AUTHORIZATION_CODE("authorization_code");
	
	private String grantType;
	
	AuthorizedGrantType(String grantType) {
        this.grantType = grantType;
    }
 
    public String getGrantType() {
        return grantType;
    }
    
    @Override
    public String toString() {
        return grantType;
    }
}