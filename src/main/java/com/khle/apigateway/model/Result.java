package com.khle.apigateway.model;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Result {
	ArrayList<Content> result;

	public Result() {
		super();
	}

	public ArrayList<Content> getResult() {
		return result;
	}

	public void setResult(ArrayList<Content> result) {
		this.result = result;
	}

	public void update() {
		for (int i = 0; i < this.result.size(); i++) {
			this.result.get(i).setTests();
		}
	}

}
