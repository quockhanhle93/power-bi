package com.khle.apigateway.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
@JsonSerialize
public class Response {
	Set<Content> content;

	public Response() {
		super();
		content = new HashSet<>();
	}

	public Set<Content> getContent() {
		return content;
	}

	public void setContent(Set<Content> content) {
		this.content = content;
	}

	public void update() {
		this.content.forEach((data)->{
			data.setTests();
		});
	}

}
