package com.khle.apigateway.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Content {
	@JsonSerialize
	private Map<String, Object> contents;
	@Id
	private Integer id;
	private Boolean expired;
	private Boolean active;
	@JsonDeserialize
	private ArrayList<Field> fields;
	
	public Content() {
		super();
		this.contents = new HashMap<>();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getExpired() {
		return expired;
	}
	public void setExpired(Boolean expired) {
		this.expired = expired;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public ArrayList<Field> getFields() {
		return fields;
	}
	public void setFields(ArrayList<Field> fields) {
		this.fields = fields;
	}
	
	public Map<String, Object> getContents() {
		return contents;
	}
	public void setContents(Map<String, Object> contents) {
		this.contents = contents;
	}
	public void setTests() {
		for (Field f : fields) {
			if(f.getOptions().size() > 0) {
				this.contents.put(f.getName(), f.getOptions().get(0).getValue());
			}			
		}
		this.setFields(null);
		
	}
}
