package com.khle.apigateway.model;

public class OptionPair {
	private String key;
    private Object value;
     
    @Override
    public String toString() {
        return key + " and " + value;
    }

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
    
}
