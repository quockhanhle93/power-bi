package com.khle.apigateway.model;

import java.util.ArrayList;

public class Field {
	private ArrayList<Option> options;
	private Integer fieldDefinitionId;
	private String name;
	public Field() {
		super();
	}
	public ArrayList<Option> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<Option> options) {
		this.options = options;
	}
	public Integer getFieldDefinitionId() {
		return fieldDefinitionId;
	}
	public void setFieldDefinitionId(Integer fieldDefinitionId) {
		this.fieldDefinitionId = fieldDefinitionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
