package com.khle.apigateway.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Link {
	private String rel;
	private String href;
	private String hreflang;
	private String media;
	private String title;
	private String type;
	private String deprecation;
	
	
	public Link() {
		super();
	}
	public String getRel() {
		return rel;
	}
	public void setRel(String rel) {
		this.rel = rel;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getHreflang() {
		return hreflang;
	}
	public void setHreflang(String hreflang) {
		this.hreflang = hreflang;
	}
	public String getMedia() {
		return media;
	}
	public void setMedia(String media) {
		this.media = media;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDeprecation() {
		return deprecation;
	}
	public void setDeprecation(String deprecation) {
		this.deprecation = deprecation;
	}
	

	
}
