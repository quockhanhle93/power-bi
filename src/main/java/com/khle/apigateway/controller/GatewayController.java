package com.khle.apigateway.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.khle.apigateway.configuration.ApiGatewayProperties;
import com.khle.apigateway.model.Response;
import com.khle.apigateway.model.Result;
import com.khle.apigateway.service.TaskTimerRepository;
import com.khle.apigateway.util.ContentRequestTransformer;
import com.khle.apigateway.util.HeadersRequestTransformer;
import com.khle.apigateway.util.URLRequestTransformer;

@RestController
public class GatewayController {
	@Autowired
	private TaskTimerRepository repository;
	private static final Logger logger = LogManager.getLogger(GatewayController.class);
	@Autowired
	private ApiGatewayProperties apiGatewayProperties;

	private HttpClient httpClient;
	
	protected RestTemplate restTemplate;
	
	@PostConstruct
	public void init() {
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();

		httpClient = HttpClients.custom().setConnectionManager(cm).build();
	}
	
	@RequestMapping(value = "/tccencode/rest/external/site/18/module/197/**", method = { GET})
	@ResponseBody
	public ResponseEntity<Response> taskTimerRequest(HttpServletRequest request)
			throws NoHandlerFoundException, IOException, URISyntaxException {
			
		HttpUriRequest proxiedRequest = createHttpUriRequest(request);
		 logger.info("request: {}", proxiedRequest);
		HttpResponse proxiedResponse = httpClient.execute(proxiedRequest);
		 logger.info("Response {}", proxiedResponse.getStatusLine().getStatusCode());
		if(proxiedResponse.getEntity() == null) {
			return new ResponseEntity<>(HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
		}
		Response res = new Response();
		parse2(read(proxiedResponse.getEntity().getContent()));
		
		res.getContent().addAll(repository.findAll());
		
		return new ResponseEntity<>(res,
				makeResponseHeaders(proxiedResponse),
				HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
	}
	
	@RequestMapping(value = "/**", method = { GET, PUT, DELETE })
	@ResponseBody
	public ResponseEntity<Object> proxyRequest(HttpServletRequest request)
			throws NoHandlerFoundException, IOException, URISyntaxException {
			
		HttpUriRequest proxiedRequest = createHttpUriRequest(request);
		 logger.info("request: {}", proxiedRequest);
		HttpResponse proxiedResponse = httpClient.execute(proxiedRequest);
		 logger.info("Response {}", proxiedResponse.getStatusLine().getStatusCode());
		if(proxiedResponse.getEntity() == null) {
			return new ResponseEntity<>(HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
		}
//		parse(read(proxiedResponse.getEntity().getContent()));
		return new ResponseEntity<>(parse(read(proxiedResponse.getEntity().getContent())),
				makeResponseHeaders(proxiedResponse),
				HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
	}
	
	
	
	@RequestMapping(value = "/**", method = { POST })
	@ResponseBody
	public ResponseEntity<Object> proxyRequest1(HttpServletRequest request)
			throws NoHandlerFoundException, IOException, URISyntaxException {
			
		HttpUriRequest proxiedRequest = createHttpUriRequest(request);
		 logger.info("request: {}", proxiedRequest);
		HttpResponse proxiedResponse = httpClient.execute(proxiedRequest);
		 logger.info("Response {}", proxiedResponse.getStatusLine().getStatusCode());
		if(proxiedResponse.getEntity() == null) {
			return new ResponseEntity<>(HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
		}
//		parse(read(proxiedResponse.getEntity().getContent()));
		return new ResponseEntity<>(parse1(read(proxiedResponse.getEntity().getContent())),
				makeResponseHeaders(proxiedResponse),
				HttpStatus.valueOf(proxiedResponse.getStatusLine().getStatusCode()));
	}

	private HttpHeaders makeResponseHeaders(HttpResponse response) {
		HttpHeaders result = new HttpHeaders();
		Optional.ofNullable(response.getFirstHeader("Content-Type"))
				.ifPresent((header) -> result.set(header.getName(), header.getValue()));
		return result;
	}

	@SuppressWarnings("unchecked")
	private HttpUriRequest createHttpUriRequest(HttpServletRequest request)
			throws URISyntaxException, NoHandlerFoundException, IOException {
		URLRequestTransformer urlRequestTransformer = new URLRequestTransformer(this.apiGatewayProperties, null);
		ContentRequestTransformer contentRequestTransformer = new ContentRequestTransformer();
		HeadersRequestTransformer headersRequestTransformer = new HeadersRequestTransformer();
		headersRequestTransformer.setPredecessor(contentRequestTransformer);
		contentRequestTransformer.setPredecessor(urlRequestTransformer);
		return headersRequestTransformer.transform(request).build();
	}

	private String read(InputStream input) throws IOException {
		if(input == null) return "";
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		}
	}
	
	private Object parse(String a) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper()
				  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Response response = objectMapper.readValue(a, Response.class);
		response.update();
		return response;
	}
	
	private Object parse2(String a) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper()
				  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Response response = objectMapper.readValue(a, Response.class);
		response.update();
		this.repository.save(response.getContent());
		return response;
	}
	
	private Object parse1(String a) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper()
				  .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Result response = objectMapper.readValue(a, Result.class);
		response.update();
		return response;
	}
	
	@Scheduled(cron = "0 1 1 * * ?")
    public void reportCurrentTime() throws NoHandlerFoundException, URISyntaxException, IOException {
		HttpServletRequest request = new MockHttpServletRequest("GET", "/tccencode/rest/external/site/18/module/197/item/?start=1&limit=500000");
		
		
		HttpUriRequest proxiedRequest = createHttpUriRequest(request);
		 logger.info("request: {}", proxiedRequest);
		HttpResponse proxiedResponse = httpClient.execute(proxiedRequest);
		 logger.info("Response {}", proxiedResponse.getStatusLine().getStatusCode());
		Response res = new Response();
		parse2(read(proxiedResponse.getEntity().getContent()));
		System.out.println("======== Done ===========");
    }
}
